package en.natera.triangle_service.model;

import java.util.Objects;

public class Triangle {
    private String id;
    private double firstSide;
    private double secondSide;
    private double thirdSide;

    public String getId() {
        return id;
    }

    public double getFirstSide() {
        return firstSide;
    }

    public double getSecondSide() {
        return secondSide;
    }

    public double getThirdSide() {
        return thirdSide;
    }

    public Triangle withId(String id) {
        this.id = id;
        return this;
    }

    public Triangle withFirstSide(double firstSide) {
        this.firstSide = firstSide;
        return this;
    }

    public Triangle withSecondSide(double secondSide) {
        this.secondSide = secondSide;
        return this;
    }

    public Triangle withThirdSide(double thirdSide) {
        this.thirdSide = thirdSide;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Triangle triangle = (Triangle) o;

        if (Double.compare(triangle.firstSide, firstSide) != 0) return false;
        if (Double.compare(triangle.secondSide, secondSide) != 0) return false;
        if (Double.compare(triangle.thirdSide, thirdSide) != 0) return false;
        return Objects.equals(id, triangle.id);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id != null ? id.hashCode() : 0;
        temp = Double.doubleToLongBits(firstSide);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(secondSide);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(thirdSide);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
