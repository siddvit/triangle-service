package en.natera.triangle_service.appmanager;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import en.natera.triangle_service.model.Triangle;
import en.natera.triangle_service.response.PostTriangleResponse;

public class POSTTriangleHelper {
    private final ApplicationManager app;

    public static final String TRIANGLE = "triangle";

    public POSTTriangleHelper(ApplicationManager app) {
        this.app = app;
    }

    public PostTriangleResponse crateTriangle(String separator, String input) {
        RequestSpecification requestSpecification = RestAssured.given();
        requestSpecification.header("X-User", app.getProperty("auth.token"))
                .header("Content-Type", "application/json");

        if (separator == null) {
            requestSpecification.body(String.format("{\"input\": \"%s\"}", input));
        }
        if (input == null) {
            requestSpecification.body(String.format("{\"separator\": \"%s\"}", separator));
        }
        if (separator != null && input != null) {
            requestSpecification.body(String.format("{\"separator\": \"%s\", \"input\": \"%s\"}", separator, input));
        }
        if (separator == null && input == null) {
            requestSpecification.body("{}");
        }

        requestSpecification.log().all();
        Response response = requestSpecification.post(app.getProperty("url.host") + TRIANGLE);
        response.then().log().body();

        return new PostTriangleResponse(response);
    }

    public Triangle crateTriangleAndGetObject(String separator, String input) {
        RequestSpecification requestSpecification = RestAssured.given();
        requestSpecification.header("X-User", app.getProperty("auth.token"))
                .header("Content-Type", "application/json");

        if (separator == null) {
            requestSpecification.body(String.format("{\"input\": \"%s\"}", input));
        }
        if (input == null) {
            requestSpecification.body(String.format("{\"separator\": \"%s\"}", separator));
        }
        if (separator != null && input != null) {
            requestSpecification.body(String.format("{\"separator\": \"%s\", \"input\": \"%s\"}", separator, input));
        }
        if (separator == null && input == null) {
            requestSpecification.body("{}");
        }

        requestSpecification.log().all();
        Response response = requestSpecification.post(app.getProperty("url.host") + TRIANGLE);
        response.then().log().body();

        Triangle newTriangle = new Triangle();
        newTriangle.withId(response.jsonPath().get("id"));
        newTriangle.withFirstSide(Double.parseDouble(response.jsonPath().getString("firstSide")));
        newTriangle.withSecondSide(Double.parseDouble(response.jsonPath().getString("secondSide")));
        newTriangle.withThirdSide(Double.parseDouble(response.jsonPath().getString("thirdSide")));

        return newTriangle;
    }
}
