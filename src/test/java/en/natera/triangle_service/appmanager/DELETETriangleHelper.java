package en.natera.triangle_service.appmanager;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import en.natera.triangle_service.response.DeleteTriangleResponse;

import static en.natera.triangle_service.appmanager.POSTTriangleHelper.TRIANGLE;

public class DELETETriangleHelper {
    private final ApplicationManager app;

    public DELETETriangleHelper(ApplicationManager app) {
        this.app = app;
    }

    public DeleteTriangleResponse deleteTriangle(String triangleId) {
        RequestSpecification requestSpecification = RestAssured.given();
        requestSpecification.header("X-User", app.getProperty("auth.token"))
                .header("Content-Type", "application/json");

        requestSpecification.log().all();

        Response response = null;

        if (triangleId == null) {
            response = requestSpecification.delete(app.getProperty("url.host") + TRIANGLE + "/");
        }
        if (triangleId != null) {
            response = requestSpecification.delete(String.format(app.getProperty("url.host") + TRIANGLE + "/%s", triangleId));
        }
        response.then().log().body();

        return new DeleteTriangleResponse(response);
    }
}
