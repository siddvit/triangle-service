package en.natera.triangle_service.appmanager;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import en.natera.triangle_service.response.GETTrianglePerimeterResponse;

import static en.natera.triangle_service.appmanager.POSTTriangleHelper.TRIANGLE;

public class GETTrianglePerimeterHelper {
    private final ApplicationManager app;
    public static final String TRIANGLE_PERIMETER = "perimeter";

    public GETTrianglePerimeterHelper(ApplicationManager app) {
        this.app = app;
    }

    public GETTrianglePerimeterResponse getPerimeterTriangle(String id) {
        Response response = null;

        RequestSpecification requestSpecification = RestAssured.given();
        requestSpecification.header("X-User", app.getProperty("auth.token"))
                .header("Content-Type", "application/json");

        requestSpecification.log().all();
        if (id != null) {
            response = requestSpecification.get(String.format(app.getProperty("url.host") + TRIANGLE + "/%s/" + TRIANGLE_PERIMETER, id));
        }
        if (id == null) {
            response = requestSpecification.get(app.getProperty("url.host") + TRIANGLE + "/" + TRIANGLE_PERIMETER);
        }
        response.then().log().body();

        return new GETTrianglePerimeterResponse(response);
    }
}
