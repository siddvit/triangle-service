package en.natera.triangle_service.appmanager;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ApplicationManager {
    public final Properties properties;

    private POSTTriangleHelper postTH;
    private DELETETriangleHelper delTH;
    private GETTriangleHelper getTH;
    private GETTrianglePerimeterHelper getPT;
    private GETTriangleAreaHelper getAT;

    public ApplicationManager() {
        properties = new Properties();
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    public void init() throws IOException {
        String target = System.getProperty("target", "local");
        properties.load(new FileReader(new File(String.format("src/test/resources/%s.properties", target))));
    }

    public POSTTriangleHelper postTH() {
        if (postTH == null) {
            postTH = new POSTTriangleHelper(this);
        }
        return postTH;
    }

    public DELETETriangleHelper delTH() {
        if (delTH == null) {
            delTH = new DELETETriangleHelper(this);
        }
        return delTH;
    }

    public GETTriangleHelper getTH() {
        if (getTH == null) {
            getTH = new GETTriangleHelper(this);
        }
        return getTH;
    }

    public GETTrianglePerimeterHelper getPT() {
        if (getPT == null) {
            getPT = new GETTrianglePerimeterHelper(this);
        }
        return getPT;
    }

    public GETTriangleAreaHelper getAT() {
        if (getAT == null) {
            getAT = new GETTriangleAreaHelper(this);
        }
        return getAT;
    }
}
