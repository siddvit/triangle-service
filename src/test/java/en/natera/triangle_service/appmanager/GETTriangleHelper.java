package en.natera.triangle_service.appmanager;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import en.natera.triangle_service.model.Triangle;
import en.natera.triangle_service.response.GetTriangleResponse;

import java.util.Set;

import static en.natera.triangle_service.appmanager.POSTTriangleHelper.TRIANGLE;

public class GETTriangleHelper {
    private final ApplicationManager app;

    public static final String TRIANGLE_ALL = "triangle/all";

    public GETTriangleHelper(ApplicationManager app) {
        this.app = app;
    }

    public Triangle getTriangle() {
        RequestSpecification requestSpecification = RestAssured.given();
        requestSpecification.header("X-User", app.getProperty("auth.token"))
                .header("Content-Type", "application/json");

        requestSpecification.log().all();
        Response response = requestSpecification.get(app.getProperty("url.host") + TRIANGLE_ALL);
        response.then().log().body();

        Triangle newTriangle = new Triangle();
        newTriangle.withId(response.jsonPath().getString("[0].id"));
        newTriangle.withFirstSide(Double.parseDouble(response.jsonPath().getString("[0].firstSide")));
        newTriangle.withSecondSide(Double.parseDouble(response.jsonPath().getString("[0].secondSide")));
        newTriangle.withThirdSide(Double.parseDouble(response.jsonPath().getString("[0].thirdSide")));

        return newTriangle;
    }

    public Set<Triangle> getTrianglesAll() {
        String json = RestAssured.given()
                .header("X-User", app.getProperty("auth.token"))
                .get(app.getProperty("url.host") + TRIANGLE_ALL).asString();

        JsonElement parsed = new JsonParser().parse(json);
        JsonElement triangles = parsed.getAsJsonArray();

        return new Gson().fromJson(triangles, new TypeToken<Set<Triangle>>() {
        }.getType());
    }

    public GetTriangleResponse getCurrentTriangle(String id) {
        Response response = null;

        RequestSpecification requestSpecification = RestAssured.given();
        requestSpecification.header("X-User", app.getProperty("auth.token"))
                .header("Content-Type", "application/json");

        requestSpecification.log().all();
        if (id != null) {
            response = requestSpecification.get(String.format(app.getProperty("url.host") + TRIANGLE + "/%s", id));
        }
        if (id == null) {
            response = requestSpecification.get(app.getProperty("url.host") + TRIANGLE + "/");
        }
        response.then().log().body();

        return new GetTriangleResponse(response);
    }
}
