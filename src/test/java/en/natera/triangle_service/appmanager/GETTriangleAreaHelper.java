package en.natera.triangle_service.appmanager;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import en.natera.triangle_service.response.GETTriangleAreaResponse;

import static en.natera.triangle_service.appmanager.POSTTriangleHelper.TRIANGLE;

public class GETTriangleAreaHelper {
    private final ApplicationManager app;
    public static final String TRIANGLE_AREA = "area";

    public GETTriangleAreaHelper(ApplicationManager app) {
        this.app = app;
    }

    public GETTriangleAreaResponse getAreaTriangle(String id) {
        Response response = null;

        RequestSpecification requestSpecification = RestAssured.given();
        requestSpecification.header("X-User", app.getProperty("auth.token"))
                .header("Content-Type", "application/json");

        requestSpecification.log().all();
        if (id != null) {
            response = requestSpecification.get(String.format(app.getProperty("url.host") + TRIANGLE + "/%s/" + TRIANGLE_AREA, id));
        }
        if (id == null) {
            response = requestSpecification.get(app.getProperty("url.host") + TRIANGLE + "/" + TRIANGLE_AREA);
        }
        response.then().log().body();

        return new GETTriangleAreaResponse(response);
    }
}
