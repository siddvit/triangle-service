package en.natera.triangle_service;

import en.natera.triangle_service.model.Triangle;
import en.natera.triangle_service.response.GetTriangleResponse;
import en.natera.triangle_service.response.PostTriangleResponse;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class TriangleGetCurrentTests extends TestBase {

    @Test
    public void testGetCurrentTriangle() {
        PostTriangleResponse newTriangle = null;

        if (app.getTH().getTrianglesAll().size() == 0) {
            newTriangle = app.postTH().crateTriangle(";", "1;2;3");
        }

        Triangle randomTriangles = app.getTH().getTriangle();

        GetTriangleResponse currentTriangle = app.getTH().getCurrentTriangle(randomTriangles.getId());

        assertEquals(currentTriangle.getStatusCode(), 200, "Check that StatusCode is Ok.");
        assertNotEquals(currentTriangle.jsonPath().get("id").toString(), "", "Check that Id is not empty.");
        assertTrue(currentTriangle.responseTime() < 3, "Check that time is less that 3s.");
        currentTriangle.checkSidesInBody(String.valueOf((float) 1.0), String.valueOf((float) 2.0), String.valueOf((float) 3.0));

        if (newTriangle.jsonPath().get("id") != null) {
            app.delTH().deleteTriangle(newTriangle.jsonPath().get("id").toString());
        }
    }

    @Test
    public void testGetCurrentTriangleWithoutId() {
        GetTriangleResponse currentTriangle = app.getTH().getCurrentTriangle(null);

        assertEquals(currentTriangle.getStatusCode(), 405, "Check that StatusCode is Ok.");
        assertTrue(currentTriangle.responseTime() < 3, "Check that time is less that 3s.");
        currentTriangle.errorMessageChecker("405", "Method Not Allowed",
                "org.springframework.web.HttpRequestMethodNotSupportedException",
                "Request method 'GET' not supported", "/triangle/");
    }

    @Test
    public void testGetCurrentTriangleWithNonexistentId() {
        GetTriangleResponse currentTriangle = app.getTH().getCurrentTriangle("12345");

        assertEquals(currentTriangle.getStatusCode(), 404, "Check that StatusCode is Ok.");
        assertTrue(currentTriangle.responseTime() < 3, "Check that time is less that 3s.");
        currentTriangle.errorMessageChecker("404", "Not Found",
                "com.natera.test.triangle.exception.NotFounException",
                "Not Found", "/triangle/12345");
    }
}
