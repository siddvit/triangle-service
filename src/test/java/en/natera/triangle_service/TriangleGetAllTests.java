package en.natera.triangle_service;

import en.natera.triangle_service.model.Triangle;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.util.Set;

import static org.testng.Assert.assertEquals;

public class TriangleGetAllTests extends TestBase {
    Triangle newTriangle;
    double firstSide = 1.01;
    double secondSide = 2.99;
    double thirdSide = 3.01;
    String separator = ";";

    @Test
    public void testGetAll() {
        Set<Triangle> oldTriangles = app.getTH().getTrianglesAll();

        newTriangle = app.postTH().crateTriangleAndGetObject(separator, String.format("%s%s%s%s%s", firstSide, separator, secondSide, separator, thirdSide));

        Set<Triangle> newTriangles = app.getTH().getTrianglesAll();

        oldTriangles.add(newTriangle);
        assertEquals(newTriangles, oldTriangles);
    }

    @AfterMethod
    public void clearAfter() {
        if (newTriangle.getId() != null) {
            app.delTH().deleteTriangle(newTriangle.getId());
        }
        return;
    }
}
