package en.natera.triangle_service;

import com.sun.org.glassfish.gmbal.Description;
import en.natera.triangle_service.response.PostTriangleResponse;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class TrianglePostTests extends TestBase {
    PostTriangleResponse newTriangle;
    double firstSide = 1;
    double secondSide = 2.99;
    double thirdSide = 3.000000001;
    String separator = ";";

    @Test
    public void testPOSTTriangle() {
        newTriangle = app.postTH().crateTriangle(separator, String.format("%s%s%s%s%s", firstSide, separator, secondSide, separator, thirdSide));

        assertEquals(newTriangle.getStatusCode(), 200, "Check that StatusCode is Ok.");
        assertNotEquals(newTriangle.jsonPath().get("id").toString(), "", "Check that Id is not empty.");
        assertTrue(newTriangle.responseTime() < 3, "Check that time is less that 3s.");
        newTriangle.checkSidesInBody(String.valueOf((float) firstSide), String.valueOf((float) secondSide), String.valueOf((float) thirdSide));
    }

    @Test
    public void testPOSTTriangleWithSpaceSeparator() {
        separator = " ";

        newTriangle = app.postTH().crateTriangle(separator, String.format("%s%s%s%s%s", firstSide, separator, secondSide, separator, thirdSide));

        assertEquals(newTriangle.getStatusCode(), 200, "Check that StatusCode is Ok.");
        assertNotEquals(newTriangle.jsonPath().get("id").toString(), "", "Check that Id is not empty.");
        assertTrue(newTriangle.responseTime() < 3, "Check that time is less that 3s.");
        newTriangle.checkSidesInBody(String.valueOf((float) firstSide), String.valueOf((float) secondSide), String.valueOf((float) thirdSide));
    }

    @Test
    public void testPOSTTriangleWithCommaSeparator() {
        separator = ",";

        newTriangle = app.postTH().crateTriangle(separator, String.format("%s%s%s%s%s", firstSide, separator, secondSide, separator, thirdSide));

        assertEquals(newTriangle.getStatusCode(), 200, "Check that StatusCode is Ok.");
        assertNotEquals(newTriangle.jsonPath().get("id").toString(), "", "Check that Id is not empty.");
        assertTrue(newTriangle.responseTime() < 3, "Check that time is less that 3s.");
        newTriangle.checkSidesInBody(String.valueOf((float) firstSide), String.valueOf((float) secondSide), String.valueOf((float) thirdSide));
    }

    @Test
    public void testPOSTTriangleWithoutSeparator() {
        separator = ";";

        newTriangle = app.postTH().crateTriangle(null, String.format("%s%s%s%s%s", firstSide, separator, secondSide, separator, thirdSide));

        assertEquals(newTriangle.getStatusCode(), 200, "Check that StatusCode is Ok.");
        assertNotEquals(newTriangle.jsonPath().get("id").toString(), "", "Check that Id is not empty.");
        assertTrue(newTriangle.responseTime() < 3, "Check that time is less that 3s.");
        newTriangle.checkSidesInBody(String.valueOf((float) firstSide), String.valueOf((float) secondSide), String.valueOf((float) thirdSide));
    }

    @Test(enabled = false)
    @Description("It's a bug.")
    public void testPOSTTriangleWithNegativeNumbers() {
        double firstSide = -1.01;
        double secondSide = -2.99;
        double thirdSide = -3.000000001;

        newTriangle = app.postTH().crateTriangle(null, String.format("%s%s%s%s%s", firstSide, separator, secondSide, separator, thirdSide));

        assertEquals(newTriangle.getStatusCode(), 200, "Check that StatusCode is Ok.");
        assertNotEquals(newTriangle.jsonPath().get("id").toString(), "", "Check that Id is not empty.");
        assertTrue(newTriangle.responseTime() < 3, "Check that time is less that 3s.");
        newTriangle.checkSidesInBody(String.valueOf((float) firstSide), String.valueOf((float) secondSide), String.valueOf((float) thirdSide));
    }

    @Test(enabled = false)
    @Description("It's a bug.")
    public void testPOSTTriangleWithNumbersMore17999() {
        double firstSide = 17.999;
        double secondSide = 7.99;
        double thirdSide = 10.000000001;

        newTriangle = app.postTH().crateTriangle(null, String.format("%s%s%s%s%s", firstSide, separator, secondSide, separator, thirdSide));

        assertEquals(newTriangle.getStatusCode(), 200, "Check that StatusCode is Ok.");
        assertNotEquals(newTriangle.jsonPath().get("id").toString(), "", "Check that Id is not empty.");
        assertTrue(newTriangle.responseTime() < 3, "Check that time is less that 3s.");
        newTriangle.checkSidesInBody(String.valueOf((float) firstSide), String.valueOf((float) secondSide), String.valueOf((float) thirdSide));
    }

    @Test
    @Description("It's not a bug because by default separator = ;")
    public void testPOSTTriangleWithoutSeparatorAndWithSpaceBetweenNumbers() {
        separator = " ";

        newTriangle = app.postTH().crateTriangle(null, String.format("%s%s%s%s%s", firstSide, separator, secondSide, separator, thirdSide));

        assertEquals(newTriangle.getStatusCode(), 422, "Check that StatusCode is Ok.");
        assertTrue(newTriangle.responseTime() < 3, "Check that time is less that 3s.");
        assertNotEquals(newTriangle.jsonPath().get("status"), "422", "Check that Status is 422.");
        assertNotEquals(newTriangle.jsonPath().get("error"), equals("Unprocessable Entity"), "Check that Error is correct.");
        assertNotEquals(newTriangle.jsonPath().get("exception"), equals("com.natera.test.triangle.exception.UnprocessableDataException"), "Check that Exception is correct.");
        assertNotEquals(newTriangle.jsonPath().get("message"), equals("Cannot process input"), "Check that Message is correct.");
        assertNotEquals(newTriangle.jsonPath().get("path"), equals("/triangle"), "Check that Path is correct.");
    }

    @Test
    public void testPOSTTriangleWithSeparatorWith2NumbersInInput() {
        newTriangle = app.postTH().crateTriangle(separator, String.format("%s%s%s%s", firstSide, separator, secondSide, separator));

        assertEquals(newTriangle.getStatusCode(), 422, "Check that StatusCode is Ok.");
        assertTrue(newTriangle.responseTime() < 3, "Check that time is less that 3s.");
        assertNotEquals(newTriangle.jsonPath().get("status"), "422", "Check that Status is 422.");
        assertNotEquals(newTriangle.jsonPath().get("error"), equals("Unprocessable Entity"), "Check that Error is correct.");
        assertNotEquals(newTriangle.jsonPath().get("exception"), equals("com.natera.test.triangle.exception.UnprocessableDataException"), "Check that Exception is correct.");
        assertNotEquals(newTriangle.jsonPath().get("message"), equals("Cannot process input"), "Check that Message is correct.");
        assertNotEquals(newTriangle.jsonPath().get("path"), equals("/triangle"), "Check that Path is correct.");
    }

    @Test
    public void testPOSTTriangleWithSeparatorWithoutInput() {
        newTriangle = app.postTH().crateTriangle(separator, null);

        assertEquals(newTriangle.getStatusCode(), 500, "Check that StatusCode is Ok.");
        assertTrue(newTriangle.responseTime() < 3, "Check that time is less that 3s.");
        assertNotEquals(newTriangle.jsonPath().get("status"), "500", "Check that Status is 500.");
        assertNotEquals(newTriangle.jsonPath().get("error"), equals("Internal Server Error"), "Check that Error is correct.");
        assertNotEquals(newTriangle.jsonPath().get("exception"), equals("java.lang.NullPointerException"), "Check that Exception is correct.");
        assertNotEquals(newTriangle.jsonPath().get("message"), equals("No message available"), "Check that Message is correct.");
        assertNotEquals(newTriangle.jsonPath().get("path"), equals("/triangle"), "Check that Path is correct.");
    }

    @Test
    public void testPOSTTriangleWithoutSeparatorAndWithoutInput() {
        newTriangle = app.postTH().crateTriangle(null, null);

        assertEquals(newTriangle.getStatusCode(), 500, "Check that StatusCode is Ok.");
        assertTrue(newTriangle.responseTime() < 3, "Check that time is less that 3s.");
        assertNotEquals(newTriangle.jsonPath().get("status"), "500", "Check that Status is 500.");
        assertNotEquals(newTriangle.jsonPath().get("error"), equals("Internal Server Error"), "Check that Error is correct.");
        assertNotEquals(newTriangle.jsonPath().get("exception"), equals("java.lang.NullPointerException"), "Check that Exception is correct.");
        assertNotEquals(newTriangle.jsonPath().get("message"), equals("No message available"), "Check that Message is correct.");
        assertNotEquals(newTriangle.jsonPath().get("path"), equals("/triangle"), "Check that Path is correct.");
    }

    @AfterMethod
    public void clearAfter() {
        if (newTriangle.jsonPath().get("id") != null) {
            app.delTH().deleteTriangle(newTriangle.jsonPath().get("id").toString());
        }
        return;
    }
}
