package en.natera.triangle_service;

import en.natera.triangle_service.model.Triangle;
import en.natera.triangle_service.response.GETTrianglePerimeterResponse;
import en.natera.triangle_service.response.PostTriangleResponse;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class TriangleGetPerimeterTests extends TestBase {


    @Test
    public void testGetPerimeter() {
        PostTriangleResponse newTriangle = null;

        if (app.getTH().getTrianglesAll().size() == 0) {
            newTriangle = app.postTH().crateTriangle(";", "1;2;3");
        }

        Triangle randomTriangles = app.getTH().getTriangle();

        GETTrianglePerimeterResponse trianglePerimeter = app.getPT().getPerimeterTriangle(randomTriangles.getId());

        assertEquals(trianglePerimeter.getStatusCode(), 200, "Check that StatusCode is Ok.");
        assertTrue(trianglePerimeter.responseTime() < 3, "Check that time is less that 3s.");
        trianglePerimeter.checkPerimeter("6.0");

        if (newTriangle.jsonPath().get("id") != null) {
            app.delTH().deleteTriangle(newTriangle.jsonPath().get("id").toString());
        }
    }

    @Test
    public void testGetPerimeterWithNonexistentId() {
        GETTrianglePerimeterResponse trianglePerimeter = app.getPT().getPerimeterTriangle("12345");

        assertEquals(trianglePerimeter.getStatusCode(), 404, "Check that StatusCode is Ok.");
        assertTrue(trianglePerimeter.responseTime() < 3, "Check that time is less that 3s.");
        trianglePerimeter.errorMessageChecker("404", "Not Found",
                "com.natera.test.triangle.exception.NotFounException",
                "Not Found", "/triangle/12345/perimeter");
    }

    @Test
    public void testGetPerimeterWithoutId() {
        GETTrianglePerimeterResponse trianglePerimeter = app.getPT().getPerimeterTriangle(null);

        assertEquals(trianglePerimeter.getStatusCode(), 404, "Check that StatusCode is Ok.");
        assertTrue(trianglePerimeter.responseTime() < 3, "Check that time is less that 3s.");
        trianglePerimeter.errorMessageChecker("404", "Not Found",
                "com.natera.test.triangle.exception.NotFounException",
                "Not Found", "/triangle/perimeter");
    }
}
