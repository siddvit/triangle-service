package en.natera.triangle_service;

import en.natera.triangle_service.model.Triangle;
import en.natera.triangle_service.response.DeleteTriangleResponse;
import org.testng.annotations.Test;

import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.lessThanOrEqualTo;


public class TriangleDeleteTests extends TestBase {
    @Test
    public void testDeleteTriangle() {
        logger.info("Get data about this situation rows Triangle.");
        Set<Triangle> oldSetOfTriangles = app.getTH().getTrianglesAll();

        logger.info("Check if now 0 Triangle - do 1 new.");
        if (oldSetOfTriangles.size() == 0) {
            app.postTH().crateTriangle(";", "1;2;3");
        }

        logger.info("Get again data about Triangles size.");
        oldSetOfTriangles = app.getTH().getTrianglesAll();

        logger.info("Take 1 of Triangle.");
        Triangle randomTriangles = app.getTH().getTriangle();

        logger.info("Send request for DEL with Id selected Triangle.");
        DeleteTriangleResponse response = app.delTH().deleteTriangle(randomTriangles.getId());

        logger.info("Check, that answer was 200.");
        assertThat(response.getStatusCode(), equalTo(200));
        logger.info("Check, that answer was less than 1 sec.");
        assertThat((int) response.responseTime(), lessThanOrEqualTo(5));

        logger.info("Get new set Triangle.");
        Set<Triangle> newSetOfTriangles = app.getTH().getTrianglesAll();

        logger.info("From old set of Triangles remove info about 1 record.");
        oldSetOfTriangles.remove(randomTriangles);

        logger.info("Check different between old set and new set of Triangles.");
        assertThat(oldSetOfTriangles, equalTo(newSetOfTriangles));
    }

    @Test
    public void testDeleteTriangleWithEmptyId() {
        Set<Triangle> oldSetOfTriangles = app.getTH().getTrianglesAll();

        DeleteTriangleResponse response = app.delTH().deleteTriangle(null);

        assertThat(response.getStatusCode(), equalTo(405));
        assertThat((double) response.responseTime(), lessThanOrEqualTo(8.0));
        response.errorMessageChecker("405", "Method Not Allowed",
                "org.springframework.web.HttpRequestMethodNotSupportedException",
                "Request method 'DELETE' not supported", "/triangle/");

        Set<Triangle> newSetOfTriangles = app.getTH().getTrianglesAll();

        assertThat(oldSetOfTriangles, equalTo(newSetOfTriangles));
    }

    @Test
    public void testDeleteTriangleWithNonexistentTriangleId() {
        Set<Triangle> oldSetOfTriangles = app.getTH().getTrianglesAll();

        DeleteTriangleResponse response = app.delTH().deleteTriangle("132456");

        assertThat(response.getStatusCode(), equalTo(200));
        assertThat((double) response.responseTime(), lessThanOrEqualTo(8.0));

        Set<Triangle> newSetOfTriangles = app.getTH().getTrianglesAll();

        assertThat(oldSetOfTriangles, equalTo(newSetOfTriangles));
    }
}
