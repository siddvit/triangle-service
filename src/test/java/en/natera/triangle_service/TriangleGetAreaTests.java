package en.natera.triangle_service;

import en.natera.triangle_service.model.Triangle;
import en.natera.triangle_service.response.GETTriangleAreaResponse;
import en.natera.triangle_service.response.PostTriangleResponse;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class TriangleGetAreaTests extends TestBase {

    @Test
    public void testGetArea() {
        PostTriangleResponse newTriangle = null;

        if (app.getTH().getTrianglesAll().size() == 0) {
            newTriangle = app.postTH().crateTriangle(";", "3;2;3");
        }

        Triangle randomTriangles = app.getTH().getTriangle();

        GETTriangleAreaResponse triangleArea = app.getAT().getAreaTriangle(randomTriangles.getId());

        assertEquals(triangleArea.getStatusCode(), 200, "Check that StatusCode is Ok.");
        assertTrue(triangleArea.responseTime() < 3, "Check that time is less that 3s.");
        triangleArea.checkArea("2.828427");

        if (newTriangle.jsonPath().get("id") != null) {
            app.delTH().deleteTriangle(newTriangle.jsonPath().get("id").toString());
        }
    }

    @Test
    public void testGetAreaWithNonexistentId() {
        GETTriangleAreaResponse triangleArea = app.getAT().getAreaTriangle("12345");

        assertEquals(triangleArea.getStatusCode(), 404, "Check that StatusCode is Ok.");
        assertTrue(triangleArea.responseTime() < 3, "Check that time is less that 3s.");
        triangleArea.errorMessageChecker("404", "Not Found",
                "com.natera.test.triangle.exception.NotFounException",
                "Not Found", "/triangle/12345/area");
    }

    @Test
    public void testGetAreaWithoutId() {
        GETTriangleAreaResponse triangleArea = app.getAT().getAreaTriangle(null);

        assertEquals(triangleArea.getStatusCode(), 404, "Check that StatusCode is Ok.");
        assertTrue(triangleArea.responseTime() < 3, "Check that time is less that 3s.");
        triangleArea.errorMessageChecker("404", "Not Found",
                "com.natera.test.triangle.exception.NotFounException",
                "Not Found", "/triangle/area");
    }
}
