package en.natera.triangle_service.response;

import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;


public class CommonValidateResponse {
    protected Response response;

    public CommonValidateResponse(Response response) {
        this.response = response;
    }

    public JsonPath jsonPath() {
        return response.jsonPath();
    }

    public int getStatusCode() {
        return response.getStatusCode();
    }

    public long responseTime() {
        return response.timeIn(TimeUnit.SECONDS);
    }

    public void statusInBody(String status) {
        assertEquals(response.jsonPath().get("status").toString(), status, "Check Status.");
    }

    public void errorInBody(String error) {
        assertEquals(response.jsonPath().get("error").toString(), error, "Check Error.");
    }

    public void exceptionInBody(String exception) {
        assertEquals(response.jsonPath().get("exception").toString(), exception, "Check Exception.");
    }

    public void messageInBody(String message) {
        assertEquals(response.jsonPath().get("message").toString(), message, "Check Message.");
    }

    public void pathInBody(String path) {
        assertEquals(response.jsonPath().get("path").toString(), path, "Check Path.");
    }

    public void errorMessageChecker(String status, String error, String exception, String message, String path) {
        statusInBody(status);
        errorInBody(error);
        exceptionInBody(exception);
        messageInBody(message);
        pathInBody(path);
    }
}
