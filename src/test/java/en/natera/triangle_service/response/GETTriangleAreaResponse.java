package en.natera.triangle_service.response;

import com.jayway.restassured.response.Response;

import static org.testng.Assert.assertEquals;

public class GETTriangleAreaResponse extends CommonValidateResponse {
    public GETTriangleAreaResponse(Response response) {
        super(response);
    }

    /**
     * **************
     * CHECKERS
     * ***************
     */

    public void checkArea(String result) {
        assertEquals(response.jsonPath().get("result").toString(), result, "Check Result.");
    }
}
