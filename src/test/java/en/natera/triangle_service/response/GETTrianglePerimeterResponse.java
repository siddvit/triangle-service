package en.natera.triangle_service.response;

import com.jayway.restassured.response.Response;

import static org.testng.Assert.assertEquals;

public class GETTrianglePerimeterResponse extends CommonValidateResponse {

    public GETTrianglePerimeterResponse(Response response) {
        super(response);
    }

    /**
     * **************
     * CHECKERS
     * ***************
     */

    public void checkPerimeter(String result) {
        assertEquals(response.jsonPath().get("result").toString(), result, "Check Result.");
    }
}
