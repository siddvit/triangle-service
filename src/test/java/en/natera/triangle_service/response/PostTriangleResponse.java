package en.natera.triangle_service.response;

import com.jayway.restassured.response.Response;

import static org.testng.Assert.assertEquals;

public class PostTriangleResponse extends CommonValidateResponse {
    public PostTriangleResponse(Response response) {
        super(response);
    }

    /**
     * **************
     * CHECKERS
     * ***************
     */

    public void checkFirstSide(String firstSide) {
        assertEquals(response.jsonPath().get("firstSide").toString(), firstSide, "Check FirstSide.");
    }

    public void checkSecondSide(String secondSide) {
        assertEquals(response.jsonPath().get("secondSide").toString(), secondSide, "Check SecondSide.");
    }

    public void checkThirdSide(String thirdSide) {
        assertEquals(response.jsonPath().get("thirdSide").toString(), thirdSide, "Check ThirdSide.");
    }

    public void checkSidesInBody(String firstSide, String secondSide, String thirdSide) {
        checkFirstSide(firstSide);
        checkSecondSide(secondSide);
        checkThirdSide(thirdSide);
    }
}
